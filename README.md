Run with parameters from the exercise.
Typical command:

java -jar transaction-generator.jar -customerIds 1:20 -dateRange "2018-03-08T00:00:00.000-0100":"2018-03-08T23:59:59.999-0100" -itemsFile items.csv -itemsCount 5:15 -itemsQuantity 1:30 -eventsCount 10 -outDir ./output -format yaml


Docker:
Before running, copy properties and csv into /storage
To run in docker use:

docker build --tag generator-transakcji .
docker run generator-transakcji