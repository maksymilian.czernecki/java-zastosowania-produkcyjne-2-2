package datagenerator;

import lombok.Data;
import lombok.ToString;

import java.math.BigDecimal;

@Data
@ToString
public class Transaction {
    private final int id;
    private final String timestamp;
    private final int customer_id;
    private final Item[] items;
    private final BigDecimal sum;
}
