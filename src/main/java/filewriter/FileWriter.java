package filewriter;

import datagenerator.Transaction;

public interface FileWriter {
    void saveTransaction(Transaction transaction, int idNumber, String outDir);
}
